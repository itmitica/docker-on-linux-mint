docker-on-linux-mint

> [Manuals / Open-source projects / Docker Registry / Deploy a registry server](https://docs.docker.com/registry/deploying/)

---

[[_TOC_]]

---

#### Run a local registry

```shell
# "registry:2" is a shortcut for the longer "docker.io/library/registry:2"
sudo docker run --detached                        \
                --publish 5000:5000               \
                --restart=always                  \
                --name private-local-registry     \
              registry:2
```
<details>
  <summary></summary>

```
Unable to find image 'registry:2' locally
2: Pulling from library/registry
91d30c5bc195: Pull complete 
65d52c8ad3c4: Pull complete 
54f80cd081c9: Pull complete 
ca8951d7f653: Pull complete 
5ee46e9ce9b6: Pull complete 
Digest: sha256:8c51be2f669c82da8015017ff1eae5e5155fcf707ba914c5c7b798fbeb03b50c
Status: Downloaded newer image for registry:2
ee1444489673a448bb59e39a2d587506a13647a10a03a0b49b24c5e7e6dae87d

$ sudo docker ps -a
CONTAINER ID   IMAGE        COMMAND                  CREATED          STATUS          PORTS                                       NAMES
ee1444489673   registry:2   "/entrypoint.sh /etc…"   19 minutes ago   Up 19 minutes   0.0.0.0:5000->5000/tcp, :::5000->5000/tcp   private-local-registry

$ sudo docker images
REPOSITORY   TAG       IMAGE ID       CREATED       SIZE
registry     2         8db46f9d7550   3 weeks ago   24.2MB

$ sudo docker info
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  buildx: Docker Buildx (Docker Inc.)
    Version:  v0.10.4
    Path:     /usr/libexec/docker/cli-plugins/docker-buildx
  compose: Docker Compose (Docker Inc.)
    Version:  v2.17.2
    Path:     /usr/libexec/docker/cli-plugins/docker-compose

Server:
 Containers: 1
  Running: 1
  Paused: 0
  Stopped: 0
 Images: 1
 Server Version: 23.0.4
 Storage Driver: overlay2
  Backing Filesystem: extfs
  Supports d_type: true
  Using metacopy: false
  Native Overlay Diff: true
  userxattr: false
 ging Driver: json-file
 Cgroup Driver: systemd
 Cgroup Version: 2
 Plugins:
  Volume: local
  Network: bridge host ipvlan macvlan null overlay
  : awss fluentd gcps gelf journald json-file local entries splunk sys
 Swarm: inactive
 Runtimes: io.containerd.runc.v2 runc
 Default Runtime: runc
 Init Binary: docker-init
 containerd version: 2806fc1057397dbaeefbea0e4e17bddfbd388f38
 runc version: v1.1.5-0-gf19387a
 init version: de40ad0
 Security Options:
  apparmor
  seccomp
   Profile: builtin
  cgroupns
 Kernel Version: 5.15.0-70-generic
 Operating System: Linux Mint 21.1
 OSType: linux
 Architecture: x86_64
 CPUs: 6
 Total Memory: 31.13GiB
 Name: hp
 ID: f60deeb1-2bb4-47b7-b1d7-fa39b6c20efe
 Docker Root Dir: /var/lib/docker
 Debug Mode: false
 Registry: https://index.docker.io/v1/
 Experimental: false
 Insecure Registries:
  127.0.0.0/8
 Live Restore Enabled: false
```
</details>


             
#### Copy an image from Docker Hub to your registry

```shell
# Pull the ubuntu:16.04 image from Docker Hub.
sudo docker pull ubuntu:16.04
```
<details>
  <summary></summary>
  
```
16.04: Pulling from library/ubuntu
58690f9b18fc: Pull complete 
b51569e7c507: Pull complete 
da8ef40b9eca: Pull complete 
fb15d46c38dc: Pull complete 
Digest: sha256:1f1a2d56de1d604801a9671f301190704c25d604a416f59e03c04f5c6ffee0d6
Status: Downloaded newer image for ubuntu:16.04
docker.io/library/ubuntu:16.04
```
</details>



```shell
# Tag the image as localhost:5000/my-ubuntu.
# This creates an additional tag for the existing image.
# When the first part of the tag is a hostname and port,
# Docker interprets this as the location of a registry, when pushing.
sudo docker tag ubuntu:16.04 localhost:5000/my-ubuntu

# Push the image to the local registry running at localhost:5000:
sudo docker push localhost:5000/my-ubuntu
```
<details>
  <summary></summary>
  
```
Using default tag: latest
The push refers to repository [localhost:5000/my-ubuntu]
1251204ef8fc: Pushed 
47ef83afae74: Pushed 
df54c846128d: Pushed 
be96a3f634de: Pushed 
latest: digest: sha256:a3785f78ab8547ae2710c89e627783cfa7ee7824d3468cae6835c9f4eae23ff7 size: 1150

$ sudo docker image ls --no-trunc
REPOSITORY                 TAG       IMAGE ID                                                                  CREATED         SIZE
registry                   2         sha256:8db46f9d755043e6c427912d5c36b4375d68d31ab46ef9782fef06bdee1ed2cd   3 weeks ago     24.2MB
ubuntu                     16.04     sha256:b6f50765242581c887ff1acc2511fa2d885c52d8fb3ac8c4bba131fd86567f2e   20 months ago   135MB
localhost:5000/my-ubuntu   latest    sha256:b6f50765242581c887ff1acc2511fa2d885c52d8fb3ac8c4bba131fd86567f2e   20 months ago   135MB
```
</details>



```shell
# Remove the locally-cached ubuntu:16.04 and localhost:5000/my-ubuntu images,
# so that you can test pulling the image from your registry.
# This does not remove the localhost:5000/my-ubuntu image from your registry.
sudo docker image remove ubuntu:16.04              ; \
sudo docker image remove localhost:5000/my-ubuntu
```
<details>
  <summary></summary>
  
```shell
Untagged: ubuntu:16.04
Untagged: ubuntu@sha256:1f1a2d56de1d604801a9671f301190704c25d604a416f59e03c04f5c6ffee0d6
Untagged: localhost:5000/my-ubuntu:latest
Untagged: localhost:5000/my-ubuntu@sha256:a3785f78ab8547ae2710c89e627783cfa7ee7824d3468cae6835c9f4eae23ff7
Deleted: sha256:b6f50765242581c887ff1acc2511fa2d885c52d8fb3ac8c4bba131fd86567f2e
Deleted: sha256:0214f4b057d78b44fd12702828152f67c0ce115f9346acc63acdf997cab7e7c8
Deleted: sha256:1b9d0485372c5562fa614d5b35766f6c442539bcee9825a6e90d1158c3299a61
Deleted: sha256:3c0f34be6eb98057c607b9080237cce0be0b86f52d51ba620dc018a3d421baea
Deleted: sha256:be96a3f634de79f523f07c7e4e0216c28af45eb5776e7a6238a2392f71e01069

$ sudo docker image ls --no-trunc
REPOSITORY   TAG       IMAGE ID                                                                  CREATED       SIZE
registry     2         sha256:8db46f9d755043e6c427912d5c36b4375d68d31ab46ef9782fef06bdee1ed2cd   3 weeks ago   24.2MB
```
</details>



```shell
# Pull the localhost:5000/my-ubuntu image from your local registry.
sudo docker pull localhost:5000/my-ubuntu
```
<details>
  <summary></summary>
  
```
Using default tag: latest
latest: Pulling from my-ubuntu
58690f9b18fc: Pull complete 
b51569e7c507: Pull complete 
da8ef40b9eca: Pull complete 
fb15d46c38dc: Pull complete 
Digest: sha256:a3785f78ab8547ae2710c89e627783cfa7ee7824d3468cae6835c9f4eae23ff7
Status: Downloaded newer image for localhost:5000/my-ubuntu:latest
localhost:5000/my-ubuntu:latest

$ sudo docker image ls --no-trunc
REPOSITORY                 TAG       IMAGE ID                                                                  CREATED         SIZE
registry                   2         sha256:8db46f9d755043e6c427912d5c36b4375d68d31ab46ef9782fef06bdee1ed2cd   3 weeks ago     24.2MB
localhost:5000/my-ubuntu   latest    sha256:b6f50765242581c887ff1acc2511fa2d885c52d8fb3ac8c4bba131fd86567f2e   20 months ago   135MB
```
</details>



#### Stop a local registry

```shell
# To stop the registry, use the same docker container stop command as with any other container.
sudo docker container stop private-local-registry
```
<details>
  <summary></summary>

```
$ sudo docker image remove localhost:5000/my-ubuntu
Untagged: localhost:5000/my-ubuntu:latest
Untagged: localhost:5000/my-ubuntu@sha256:a3785f78ab8547ae2710c89e627783cfa7ee7824d3468cae6835c9f4eae23ff7
Deleted: sha256:b6f50765242581c887ff1acc2511fa2d885c52d8fb3ac8c4bba131fd86567f2e
Deleted: sha256:0214f4b057d78b44fd12702828152f67c0ce115f9346acc63acdf997cab7e7c8
Deleted: sha256:1b9d0485372c5562fa614d5b35766f6c442539bcee9825a6e90d1158c3299a61
Deleted: sha256:3c0f34be6eb98057c607b9080237cce0be0b86f52d51ba620dc018a3d421baea
Deleted: sha256:be96a3f634de79f523f07c7e4e0216c28af45eb5776e7a6238a2392f71e01069

$ sudo docker container stop private-local-registry
private-local-registry

```
</details>



```shell
# To remove the container, use docker container rm.
sudo docker container stop private-local-registry           && \
sudo docker container rm --volumes private-local-registry
```
<details>
  <summary></summary>

```
$ sudo docker image remove registry:2
Untagged: registry:2
Untagged: registry@sha256:8c51be2f669c82da8015017ff1eae5e5155fcf707ba914c5c7b798fbeb03b50c
Deleted: sha256:8db46f9d755043e6c427912d5c36b4375d68d31ab46ef9782fef06bdee1ed2cd
Deleted: sha256:77772f9a7bb9df7f2d25e374dcbda41b9025b4ce5a26f17d10043d39d9bcafac
Deleted: sha256:e5d7cfb455bd379332a3da31db783040de85cae7e981f05ae1388e11ca8b0e46
Deleted: sha256:b7109a1a0d45f2335e66af07f5161c4181565bf6b4bb7541e456ba15e1f62019
Deleted: sha256:5e85a660413809aa0f42ad3ed494f7a3b40fdf5349d3a6c30cbd41b6dacdd10c
Deleted: sha256:5bc340f6d4f5a3bc999dfbc790a0bdf0920b9103ef794645034de4260ee4e9c8
```
</details>
