```shell
# Data peristence
#   Named volumes - from  inside container  to  inside container: C2C
#   Bind mounts   - from  file system       to  inside container: Fs2C

# Named volumes
#   used when data inside containers needs to be shared with other containers
#   named volume is populated with container content
#   managed by Docker
type=volume ,src=named-volume  ,target=/usr/local/data

# Bind mounts
#   used when data outside containers needs to be accessed inside the containers
#   bind mount is not populated with container content
#   managed by the file system
type=bind   ,src=/path/to/data ,target=/usr/local/data
