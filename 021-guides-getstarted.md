# docker-on-linux-mint


## Get Started



### [Guides / Get started / Part 2: Containerize an application](https://docs.docker.com/get-started/02_our_app/)


```shell
# Build the app’s container image


# Change directory to the app directory
cd /path/to/app


# Create an empty file named Dockerfile.
touch Dockerfile


# Using a text editor or code editor, add contents to the Dockerfile
...


# Build the container image using the following commands:
sudo docker build -t getting-started .


# List images
sudo docker images


# Start your container using the docker run command and specify the name of the image you just created:
# Regardless of the EXPOSE settings in the Dockerfile, you can override the ports at runtime by using the -p flag
# to create a mapping between the host’s port 3000 to the container’s port 3000.
# Without the port mapping, you wouldn’t be able to access the application
sudo docker run -dp 3000:3000 getting-started


# Run the following docker ps command in a terminal to list your containers.
sudo docker ps

```



### [Guides / Get started / Part 3: Update the application](https://docs.docker.com/get-started/03_updating_app/)


```shell
# Use the docker stop command to stop the container.
sudo docker stop <the-container-id>


# Once the container has stopped, you can remove it by using the docker rm command.
sudo docker rm <the-container-id>


# You can stop and remove a container in a single command by adding the force flag to the docker rm command.
sudo docker rm -f <the-container-id>

```



### [Guides / Get started / Part 4: Share the application](https://docs.docker.com/get-started/04_sharing_app/)


```shell
# To share Docker images, you have to use a Docker registry. The default registry is Docker Hub


# Use the docker tag command to give the getting-started image a new name.
sudo docker tag getting-started YOUR-USER-NAME/getting-started


# This command will push to this repo on Docker Hub.
# If you don’t specify a tag, Docker will use a tag called latest.
sudo docker push YOUR-USER-NAME/getting-started


# To build an image for the amd64 platform, use the --platform flag.
sudo docker build --platform linux/amd64 -t YOUR-USER-NAME/getting-started .

```



### [Guides / Get started / Part 5: Persist the DB](https://docs.docker.com/get-started/05_persisting_data/)


```shell
# The container’s filesystem:
#   use docker ps to get the container’s ID
#   use the docker exec command to access the container
sudo docker exec <container-id> cat /data.txt


# Volumes are stored in a part of the host filesystem which is managed by Docker
# and are isolated from the core functionality of the host machine.


# When that container stops or is removed, the volume still exists.
# Multiple containers can mount the same volume simultaneously, either read-write or read-only.
# Volumes are only removed when you explicitly remove them.


# If you need to back up, restore, or migrate data from one Docker host to another, volumes are a better choice.
# You can stop containers using the volume, then back up the volume’s directory (such as /var/lib/docker/volumes/<volume-name>).


# By creating a volume and attaching it (often called “mounting”) to the directory where you stored the data, you can persist the data.
# If you mount a directory in the container, changes in that directory are also seen on the host machine.
# If you mount that same directory across container restarts, you’d see the same files.


# Create a volume by using the docker volume create command.
# Docker fully manages the volume, including the storage location on disk.
# You only need to remember the name of the volume.
sudo docker volume create todo-db


# Start the todo app container, but add the --mount option to specify a volume mount.
# Give the volume a name, and mount it to /etc/todos in the container, which captures all files created at the path.
# [host, /var/lib/docker/volumes/todo-db] is volume mounted for [container, /etc/todos] 
sudo docker run -dp 3000:3000 --mount type=volume,src=todo-db,target=/etc/todos getting-started


# Dive into the volume mount:
# The volume mountpoint is the actual location of the data on the host disk: /var/lib/docker/volumes/todo-db/_data
sudo docker volume inspect todo-db

```



### [Guides / Get started / Part 6: Use bind mounts](https://docs.docker.com/get-started/06_bind_mounts/)


```shell
# A bind mount is another type of mount, which lets you share a directory from the host’s filesystem into the container.


# The file or directory does not need to exist on the Docker host already.
# It is created on demand if it does not yet exist.
# Bind mounts are very performant, but they rely on the host machine’s filesystem having a specific directory structure available. 


# Run a Container Interactively
sudo docker container run -it [docker_image] /bin/sh


# Interactive bash session in the root directory of the container’s filesystem.

# After running the command, Docker starts an interactive bash session in the root directory of the container’s filesystem.
# src is the current working directory on the host machine (getting-started/app)
# target is where that directory should appear inside the container (/src).
# Stop the interactive container session with Ctrl + D
sudo docker run -it --mount type=bind,src="$(pwd)",target=/src ubuntu bash


# Run your app in a development container

# Using bind mounts is common for local development setups.
# The advantage is that the development machine doesn’t need to have all of the build tools and environments installed.
docker run -dp 3000:3000 \
    -w /app --mount type=bind,src="$(pwd)",target=/app \
    node:18-alpine \
    sh -c "yarn install && yarn run dev"


# You can watch the logs
#   exit out by hitting Ctrl+C
docker logs -f <container-id>

```



### [Guides / Get started / Part 7: Multi-container apps](https://docs.docker.com/get-started/07_multi_container/)


```shell
# Container networking
docker network create todo-app


# Start a MySQL container and attach it to the network
docker run -d \
     --network todo-app --network-alias mysql \
     -v todo-mysql-data:/var/lib/mysql \
     -e MYSQL_ROOT_PASSWORD=secret \
     -e MYSQL_DATABASE=todos \
     mysql:8.0


# Connect to the database in the container: password is "secret"
# Verify database connection
docker exec -it <mysql-container-id> mysql -u root -p
mysql> SHOW DATABASES;


# Run your app with MySQL
# While using env vars to set connection settings is generally accepted for development,
#   it’s highly discouraged when running applications in production.
# A more secure mechanism is to use the secret support provided by your container orchestration framework.
# As an example, setting the MYSQL_PASSWORD_FILE var will cause the app
#   to use the contents of the referenced file as the connection password.
# Docker doesn’t do anything to support these env vars.
# Your app will need to know to look for the variable and get the file contents.
docker run
   -d
   -p 3000:3000 \
   -w /app -v "$(pwd):/app" \
   --network todo-app \
   -e MYSQL_HOST=mysql \
   -e MYSQL_USER=root \
   -e MYSQL_PASSWORD=secret \
   -e MYSQL_DB=todos \
   node:18-alpine \
   sh -c "yarn install && yarn run dev"


# Connect to the mysql database and prove that the items are being written to the database.
# Remember, the password is secret.
docker exec -it <mysql-container-id> mysql -p todos
mysql> select * from todo_items;

```



### [Guides / Get started / Part 9: Image-building best practices](https://docs.docker.com/get-started/09_image_best/)


```shell
# Image layering
# Use the docker image history command to see the layers in the getting-started image you created earlier in the tutorial.
# If you add the --no-trunc flag, you’ll get the full output
docker image history --no-trunc getting-started


# Layer caching
# We need to restructure our Dockerfile to help support the caching of the dependencies.
# For Node-based applications, those dependencies are defined in the package.json file.
# We copy only that file in first, install the dependencies, and then copy in everything else
# Then, we only recreate the yarn dependencies if there was a change to the package.json.
# The node_modules folder should be omitted in the second COPY step.
# Create a file named .dockerignore in the same folder as the Dockerfile with the following contents:
#  node_modules

# syntax=docker/dockerfile:1            # syntax=docker/dockerfile:1
FROM node:18-alpine                     FROM node:18-alpine
WORKDIR /app                            WORKDIR /app
                                        COPY package.json yarn.lock ./
COPY . .
RUN yarn install --production           RUN yarn install --production
                                        COPY . .
CMD ["node", "src/index.js"]            CMD ["node", "src/index.js"]


# Build the Docker image
docker build -t getting-started .
# ...
# => CACHED [2/5] WORKDIR /app
# => CACHED [3/5] COPY package.json yarn.lock ./
# => CACHED [4/5] RUN yarn install --production
# ...



# Multi-stage builds
# There are several advantages 
#   Separate build-time dependencies from runtime dependencies
#   Reduce overall image size by shipping only what your app needs to run\

# Maven/Tomcat example
# When building Java-based applications, a JDK is needed to compile the source code to Java bytecode.
# However, that JDK isn’t needed in production.
# We use one stage (called build) to perform the actual Java build using Maven.
# we copy in files from the build stage in the second stage (starting at FROM tomcat), 
# The final image is only the last stage being created (which can be overridden using the --target flag).
# syntax=docker/dockerfile:1
FROM maven AS build
WORKDIR /app
COPY . .
RUN mvn package

FROM tomcat
COPY --from=build /app/target/file.war /usr/local/tomcat/webapps

# React example
# When building React applications, we need a Node environment to compile the JS code (typically JSX),
#   SASS stylesheets, and more into static HTML, JS, and CSS.
# If we aren’t doing server-side rendering, we don’t even need a Node environment for our production build.
# Why not ship the static resources in a static nginx container?
# Here, we are using a node:18 image to perform the build (maximizing layer caching) 
#   and then copying the output into an nginx container.
# syntax=docker/dockerfile:1
FROM node:18 AS build
WORKDIR /app
COPY package* yarn.lock ./
RUN yarn install
COPY public ./public
COPY src ./src
RUN yarn run build

FROM nginx:alpine
COPY --from=build /app/build /usr/share/nginx/html

```