# docker-on-linux-mint


## [Guides / Language-specific guides / C# (.NET)Build images](https://docs.docker.com/language/dotnet/build-images/)
 


### Prerequisites: Install .NET 7.0 SDK

.NET 7 isn't provided in the default Ubuntu package feed. You'll need to add the Microsoft package repository and then install .NET.

To get the Ubuntu version Linux Mint is based on, look for `DISTRIB_RELEASE` in `/etc/upstream-release/lsb-release`



#### [Register the Microsoft package repository and install .NET 7.0 SDK](https://learn.microsoft.com/en-us/dotnet/core/install/linux-ubuntu#register-the-microsoft-package-repository)


```shell
# Uninstall .NET
sudo apt-get remove dotnet-sdk-7.0

# Get Ubuntu version
# declare repo_version=$(if command -v lsb_release &> /dev/null; then lsb_release -r -s; else grep -oP '(?<=^VERSION_ID=).+' /etc/os-release | tr -d '"'; fi)
declare repo_version=$(grep -oP '(?<=^DISTRIB_RELEASE=).+' /etc/upstream-release/lsb-release)

# Download Microsoft signing key and repository
wget https://packages.microsoft.com/config/ubuntu/$repo_version/packages-microsoft-prod.deb -O packages-microsoft-prod.deb

# Install Microsoft signing key and repository
sudo dpkg -i packages-microsoft-prod.deb

# Clean up
rm packages-microsoft-prod.deb

# Update packages
sudo apt update

# Install .NET through the package manager
sudo apt install dotnet-sdk-7.0

```

https://hamy.xyz/labs/2022-10-create-fsharp-project-from-command-line