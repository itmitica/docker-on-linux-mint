# docker-on-linux-mint


## Get Docker

### [Manuals / Docker Engine / Install / Ubuntu](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)

Linux Mint 21.1 "Vanessa" is based on Ubuntu 22.04 "Jammy Jellyfish". Hence, to adapt the instruction from Ubuntu to Linux Mint, replace `VERSION_CODENAME` with `UBUNTU_CODENAME`.

```shell
# Uninstall Docker Engine


# Stop the daemon
sudo systemctl stop docker


# Uninstall the Docker Engine, CLI, containerd, and Docker Compose packages:
sudo apt-get purge docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin docker-ce-rootless-extras


# Images, containers, volumes, or custom configuration files on your host aren’t automatically removed.
sudo rm -rf /var/lib/docker
sudo rm -rf /var/lib/containerd
sudo rm -rf /etc/apt/sources.list.d/docker.list
sudo rm -rf /etc/apt/keyrings/docker.gpg



# Install using the apt repository


# Update the apt package index and install packages to allow apt to use a repository over HTTPS:
sudo apt update
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg


# Add Docker’s official GPG key:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg


# Use the following command to set up the repository:
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$UBUNTU_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null


# Check
cat /etc/apt/sources.list.d/docker.list
# deb [arch=amd64 signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu jammy stable


# Update the apt package index:
sudo apt update


# Install Docker Engine, containerd, and Docker Compose.
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin


# Verify that the Docker Engine installation is successful by running the hello-world image:
sudo docker run hello-world
# ...
# Hello from Docker!
# This message shows that your installation appears to be working correctly.
# ...
```
