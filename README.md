# docker-on-linux-mint


[Notes](001-notes.md)

[Manuals / Docker Engine / Install / Ubuntu](011-manuals-dockerengine-install.md)

[Manuals / Open-source projects / Docker Registry / Deploy a registry server](012-manuals-dockerregistry.md)

[Guides / Get started](021-guides-getstarted.md)

[Guides / Language-specific guides/ C# (.NET)](022-guides-languagespecific-csharp.md)
